<?php
namespace AluPays\Command;

use AluPays\Entity\PaymentLinkResult;
use AluPays\Exception\AluPaysException as Exception;

class ResendPaymentLinkCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de links de pagos.
   */
  const API_ENTRYPOINT = 'payment/link';

  public function execute($params, $raw = false) {
    if (empty($params)) {
        throw new Exception('Debe suministrar una token.');
    }

    $response = $this->http_client->put($this->base_url, self::API_ENTRYPOINT."/$params", null);
    $retTx = $response->then(function ($result) use ($raw) {
          if (!$raw) {
              $retTx = new PaymentLinkResult(
                $result['result']['token'],
                $result['result']['email'],
                $result['result']['has_seen'],
                $result['result']['url']
              );

              return $retTx;
          } else {
              return $result['result'];
          }
    }, function ($result, $rawResponse) {
          if (!empty($result['message'])) {
            return $result['message'];
          } else {
            return $rawResponse;
          }
    });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
