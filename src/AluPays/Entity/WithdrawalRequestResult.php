<?php

namespace AluPays\Entity;

/**
 * Resultado de una solicitud a la API de retiros.
 */
class WithdrawalRequestResult
{
    /**
     * @var mixed El id de aluPays de la solicitud de retiro.
     */
    protected $id = null;
    /**
     * @var string El estado del retiro.
     */
    protected $state = null;
    protected $currency = null;
    protected $amount = null;
    protected $created = null;
    protected $description = null;
    protected $payeer_remarks = null;

    public function __construct($id, $state)
    {
        $this->id = $id;
        $this->state = $state;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getState()
    {
        return $this->state;
    }

    public function &setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function &setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function &setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function &setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function &setPayeerRemarks($remarks)
    {
        $this->payeer_remarks = $remarks;
        return $this;
    }

    public function getPayeerRemarks()
    {
        return $this->payeer_remarks;
    }
}
