<?php
namespace AluPays\Command;

use AluPays\Entity\AccountResult;
use AluPays\Exception\AluPaysException as Exception;

class QueryAccountsCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de cuentas.
   */
  const API_ENTRYPOINT = 'accounts';

  public function execute($options, $raw = false) {
    $response = $this->http_client->get($this->base_url, self::API_ENTRYPOINT, $options);
    $retTx = $response->then(function ($results) use ($raw) {
            if (!$raw) {
                $retTxs = [
                  'summary' => [
                    'total' => 0,
                    'page' => 1,
                    'total_per_page' => null
                  ],
                  'accounts' => []
                ];

                foreach ($results['result']['collection'] as $result) {
                    $retTx = new AccountResult(
                      $result['agency_account']['id']
                    );
                    $retTx->setBalance($result['agency_account']['balance']);
                    $retTx->setCreated($result['agency_account']['created']);
                    $retTx->setCurrency($result['agency_account']['currency']);

                    $retTxs['accounts'][] = $retTx;
                }

                $retTxs['summary']['total'] = $results['result']['total'];
                $retTxs['summary']['page'] = $results['result']['page'];
                $retTxs['summary']['total_per_page'] = $results['result']['total_per_page'];

                return $retTxs;
            } else {
                return $results['result'];
            }
      });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
