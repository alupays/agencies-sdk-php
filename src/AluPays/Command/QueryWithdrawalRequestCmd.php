<?php
namespace AluPays\Command;

use AluPays\Entity\WithdrawalRequestResult;
use AluPays\Exception\AluPaysException as Exception;

class QueryWithdrawalRequestCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de retiros.
   */
  const API_ENTRYPOINT = 'withdrawal/requests';

  public function execute($options, $raw = false) {
    $response = $this->http_client->get($this->base_url, self::API_ENTRYPOINT, $options);
    $retTx = $response->then(function ($results) use ($raw) {
            if (!$raw) {
                $retTxs = [
                  'summary' => [
                    'total' => 0,
                    'page' => 1,
                    'total_per_page' => null
                  ],
                  'withdrawal_requests' => []
                ];

                foreach ($results['result']['collection'] as $result) {
                    $retTx = new WithdrawalRequestResult(
                      $result['withdrawal_request']['id'],
                      $result['withdrawal_request']['state']
                    );
                    $retTx->setAmount($result['withdrawal_request']['amount']);
                    $retTx->setCurrency($result['withdrawal_request']['currency']);
                    $retTx->setCreated($result['withdrawal_request']['created']);
                    $retTx->setDescription($result['withdrawal_request']['description']);
                    $retTx->setPayeerRemarks($result['withdrawal_request']['payeer_remarks']);

                    $retTxs['withdrawal_requests'][] = $retTx;
                }

                $retTxs['summary']['total'] = $results['result']['total'];
                $retTxs['summary']['page'] = $results['result']['page'];
                $retTxs['summary']['total_per_page'] = $results['result']['total_per_page'];

                return $retTxs;
            } else {
                return $results['result'];
            }
      });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
