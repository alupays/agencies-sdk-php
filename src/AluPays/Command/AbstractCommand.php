<?php
namespace AluPays\Command;

abstract class AbstractCommand {
  protected $http_client = null;
  protected $base_url = null;
  protected $response = null;

  public function __construct($http_client, $base_url) {
    $this->http_client = $http_client;
    $this->base_url = $base_url;
  }

  public function getResponse()
  {
    return $this->response;
  }

  /**
   * @param mixed datos a procesar.
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un GenericResult.
   *
   * @return AluPays\Entity\GenericResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un GenericResult.
   */
  abstract public function execute($data, $raw = false);
}
