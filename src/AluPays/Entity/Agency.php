<?php

namespace AluPays\Entity;

class Agency
{
    protected $fantasy_name = null;
    protected $legal_name = null;
    protected $address = null;
    protected $logo = null;

    public function getFantasyName()
    {
        return $this->fantasy_name;
    }

    public function &setFantasyName($fantasy_name)
    {
        $this->fantasy_name = $fantasy_name;
        return $this;
    }

    public function getLegalName()
    {
        return $this->legal_name;
    }

    public function &setLegalName($legal_name)
    {
        $this->legal_name = $legal_name;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function &setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function &setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    public function toArray()
    {
        $out = [
          'fantasy_name' => $this->getFantasyName(),
          'legal_name' => $this->getLegalName(),
          'address' => $this->getAddress(),
          'logo' => $this->getLogo()
        ];
        return $out;
    }

    public function toJSON()
    {
        return json_encode($this->toArray());
    }
}
