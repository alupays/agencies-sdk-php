<?php

namespace AluPays\Core;

/**
 * Class AluPaysConstants
 * Placeholder for aluPays Constants
 *
 * @package AluPays\Core
 */
class AluPaysConstants
{
	/**
	 * @const string Cabecera de Autorización HTTP
	 */
	const HEADERS = "Authorization: AAS [TOKEN]";

	/**
	 * @const string Número de versión del PHP SDK de AluPays.
	 */
	const VERSION = '2.8.0';

	/**
	 * @const string URL base productiva de la API de aluPays.
	 */
	const API_BASE_LIVE_URL = 'https://api.alupays.com/v1/';

	/**
	 * @const string URL base sandbox de la API de aluPays.
	 */
	const API_BASE_SANDBOX_URL = 'https://apisandbox.alupays.com/v1/';
}
