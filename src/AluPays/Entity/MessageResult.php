<?php

namespace AluPays\Entity;

class MessageResult
{
    protected $message = null;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
