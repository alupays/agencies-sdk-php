<?php

namespace AluPays\Entity;

/**
 * Resultado de un movimiento de la API de movimientos.
 */
class AccountMovResult
{
    /**
     * @var mixed El id de aluPays de los movimientos.
     */
    protected $id = null;
    protected $currency = null;
    protected $amount = null;
    protected $account_id = null;
    protected $created = null;
    protected $description = null;
    protected $long_description = null;

    public function __construct($id, $account)
    {
        $this->id = $id;
        $this->currency = $account['currency'];
        $this->account_id = $account['id'];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getAccountId()
    {
        return $this->account_id;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function &setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function &setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function &setDescription($desc)
    {
        $this->description = $desc;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function &setLongDescription($desc)
    {
        $this->long_description = $desc;
        return $this;
    }

    public function getLongDescription()
    {
        return $this->long_description;
    }
}
