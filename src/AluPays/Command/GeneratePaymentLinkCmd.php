<?php
namespace AluPays\Command;

use AluPays\Entity\Transaction;
use AluPays\Entity\PaymentLinkResult;
use AluPays\Exception\AluPaysException as Exception;

class GeneratePaymentLinkCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de links de pagos.
   */
  const API_ENTRYPOINT = 'payment/link';

  public function execute($params, $raw = false) {
    if (empty($params['transaction']) &&
        !empty($params['email']) &&
        filter_var($params['email'], FILTER_VALIDATE_EMAIL)
    ) {
        throw new Exception('Debe suministrar una transacción y una dirección de email válida.');
    }

    $params['transaction']->setType(Transaction::TX_CREATE);

    $response = $this->http_client->post($this->base_url, self::API_ENTRYPOINT, json_encode([
      'transaction' => $params['transaction']->toArray(),
      'email' => $params['email']
    ]));
    $retTx = $response->then(function ($result) use ($raw) {
          if (!$raw) {
              $retTx = new PaymentLinkResult(
                $result['result']['token'],
                $result['result']['email'],
                $result['result']['has_seen'],
                $result['result']['url']
              );

              return $retTx;
          } else {
              return $result['result'];
          }
    }, function ($result, $rawResponse) {
          if (!empty($result['message'])) {
            return $result['message'];
          } else {
            return $rawResponse;
          }
    });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
