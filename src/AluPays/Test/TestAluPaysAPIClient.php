<?php

namespace AluPays\Test;

use AluPays\Authentication\APICredential;
use AluPays\Entity\Transaction;
use AluPays\Entity\TransactionResult;
use AluPays\Http\HttpClient;
use AluPays\Exception\AluPaysException as Exception;

class TestAluPaysAPIClient
{
    /**
   * @var \AluPays\Authentication\APICredential La credencial de acceso a la API.
   */
  protected $api_credential = null;

  /**
   * @var \AluPays\Http\HttpClient El cliente para generar las request HTTP.
   */
  protected $http_client = null;

  /**
   * @var string La URL de la API.
   */
  protected $apiurl = 'http://localhost/web/alupays/test/v1/';

  /**
   * @var \Psr\Http\Message\ResponseInterface Última respuesta.
   */
  protected $response = null;

  /**
   * @param string $api_key
   * @param string $api_secret
   */
  public function __construct($api_key, $api_secret)
  {
      $this->api_credential = new APICredential($api_key, $api_secret);
      $this->http_client = new HttpClient($this->api_credential);
  }

  public function getURL()
  {
      return $this->apiurl;
  }

  public function &getClient()
  {
      return $this->http_client;
  }

  public function &getLastResponse()
  {
      return $this->response;
  }

  public function settle(Transaction $transaction, $raw = false)
  {
    if (empty($transaction)) {
        throw new Exception('Debe suministrar una transacción');
    }

    $response = $this->getClient()->post($this->apiurl, 'transaction/settle', $transaction->toJSON());
    $retTx = $response->then(function ($result) use ($raw) {
          if (!$raw) {
              $retTx = new TransactionResult(
                $result['result']['transaction']['id'],
                $result['result']['transaction']['internal_id'],
                $result['result']['transaction']['status']
              );

              return $retTx;
          } else {
              return $result['result'];
          }
    }, function ($result, $rawResponse) {
          if (!empty($result['message'])) {
            return $result['message'];
          } else {
            return $rawResponse;
          }
    });
    $this->response = $response->getRawResponse();

    return $retTx;
  }

  public function settlementDecline(Transaction $transaction, $raw = false)
  {
    if (empty($transaction)) {
        throw new Exception('Debe suministrar una transacción');
    }

    $response = $this->getClient()->post($this->apiurl, 'transaction/settlement/decline', $transaction->toJSON());
    $retTx = $response->then(function ($result) use ($raw) {
          if (!$raw) {
              $retTx = new TransactionResult(
                $result['result']['transaction']['id'],
                $result['result']['transaction']['internal_id'],
                $result['result']['transaction']['status']
              );

              return $retTx;
          } else {
              return $result['result'];
          }
    }, function ($result, $rawResponse) {
          if (!empty($result['message'])) {
            return $result['message'];
          } else {
            return $rawResponse;
          }
    });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
