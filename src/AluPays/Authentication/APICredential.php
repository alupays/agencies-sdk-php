<?php

namespace AluPays\Authentication;

class APICredential
{
    /**
     * @var string La api key.
     */
    protected $api_key;

    /**
     * @var string El api secret.
     */
    protected $api_secret;

    /**
     * @param string $apiKey
     * @param string $apiSecret
     */
    public function __construct($api_key, $api_secret)
    {
        $this->api_key = $api_key;
        $this->api_secret = $api_secret;
    }

    /**
     * Retorna la api key.
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->api_key;
    }

    /**
     * Retorna el api secret.
     *
     * @return string
     */
    public function getApiSecret()
    {
        return $this->api_secret;
    }

    /**
     * Retorna el token de acceso a la API.
     *
     * @return AccessToken
     */
    public function getAccessToken()
	{
		$payload = [
			'username'	 => $this->getApiKey(),
			'signature'	 => sha1( $this->getApiSecret() ),
		];

		$aas = base64_encode( json_encode( $payload ) );
		return 'AAS ' . $aas;
    }
}
