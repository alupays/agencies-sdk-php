<?php

namespace AluPays\Entity;

/**
 * Resultado consulta API de perfil de usuario.
 */
class ProfileResult
{
    protected $user = null;
    protected $agency = null;

    public function getUser()
    {
        return $this->user;
    }

    public function &setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getAgency()
    {
        return $this->agency;
    }

    public function &setAgency($agency)
    {
        $this->agency = $agency;
        return $this;
    }
}

