<?php

namespace AluPays\Entity;

/**
 * Cliente.
 */
class Customer
{
    /**
     * @var mixed El id del cliente.
     */
    protected $id = null;
    /**
     * @var array de AluPays\Entity\CustomerField Un arreglo de campos de
     * datos asociados al cliente.
     */
    protected $fields = [];

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function &addField(CustomerField $field)
    {
        $this->fields[] = $field;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function toArray()
    {
        return [
          'id' => $this->id,
          'fields' => array_map(function ($field) {
              return $field->toArray();
        }, $this->fields),
      ];
    }

    public function toJSON()
    {
        return json_encode($this->toArray());
    }
}
