<?php

namespace AluPays;

use AluPays\Authentication\APICredential;
use AluPays\Entity\Transaction;
use AluPays\Entity\ProfileAction;
use AluPays\Entity\WithdrawalRequest;
use AluPays\Http\HttpClient;
use AluPays\Core\AluPaysConstants;
use AluPays\Command\AbstractCommand;
use AluPays\Command\GenerateTransactionCmd;
use AluPays\Command\CaptureTransactionCmd;
use AluPays\Command\RefundTransactionCmd;
use AluPays\Command\GenerateAndAuthorizeTxCmd;
use AluPays\Command\GenerateWithdrawalRequestCmd;
use AluPays\Command\GeneratePaymentLinkCmd;
use AluPays\Command\QueryTransactionCmd;
use AluPays\Command\QueryWithdrawalRequestCmd;
use AluPays\Command\QueryWithdrawalSettingsCmd;
use AluPays\Command\QueryPaymentLinkCmd;
use AluPays\Command\QueryAccountsCmd;
use AluPays\Command\QueryAccountMovsCmd;
use AluPays\Command\QueryProfileCmd;
use AluPays\Command\QueryStatsCmd;
use AluPays\Command\ResendPaymentLinkCmd;
use AluPays\Command\DeletePaymentLinkCmd;
use AluPays\Command\GenerateTokenCmd;
use AluPays\Command\EditProfileCmd;

class AluPaysAPIClient
{
    /**
   * @var \AluPays\Authentication\APICredential La credencial de acceso a la API.
   */
  protected $api_credential = null;

  /**
   * @var \AluPays\Http\HttpClient El cliente para generar las request HTTP.
   */
  protected $http_client = null;

  /**
   * @var string La URL de la API.
   */
  protected $apiurl = null;

  /**
   * @var \Psr\Http\Message\ResponseInterface Última respuesta.
   */
  protected $response = null;

  /**
   * @param string $api_key
   * @param string $api_secret
   */
  public function __construct($api_key, $api_secret)
  {
      $this->api_credential = new APICredential($api_key, $api_secret);
      $this->http_client = new HttpClient($this->api_credential);
      $this->setLiveMode();
  }

  /**
   * Establece la URL en modo Sandbox.
   */
  public function setSandboxMode()
  {
      $this->apiurl = AluPaysConstants::API_BASE_SANDBOX_URL;

      return $this;
  }

  /**
   * Establece la URL en modo Producción.
   */
  public function &setLiveMode()
  {
      $this->apiurl = AluPaysConstants::API_BASE_LIVE_URL;

      return $this;
  }

    public function getURL()
    {
        return $this->apiurl;
    }

    public function &getClient()
    {
        return $this->http_client;
    }

    public function &getLastResponse()
    {
        return $this->response;
    }

    /**
     * @param WithdrawalRequest datos de la solicitud de retiros.
     * @param bool por defecto es false, si es verdadero se retorna la salida
     * en formato JSON, caso contrario se retorna un WithdrawalRequestResult.
     *
     * @return AluPays\Entity\WithdrawalRequestResult o JSON dependiendo del parámetro $raw.
     * Por defecto retorna un WithdrawalRequestResult.
     */
    public function generateWithdrawalRequest(WithdrawalRequest $wrequest, $raw = false)
    {
        $cmd = new generateWithdrawalRequestCmd(
              $this->http_client,
              $this->getURL()
        );

        return $this->execute($cmd, $wrequest, $raw);
    }

  /**
   * @param Transaction datos de las transacción que se genera.
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un TransactionResult.
   *
   * @return AluPays\Entity\TransactionResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un TransactionResult.
   */
  public function generateTransaction(Transaction $transaction, $raw = false)
  {
      $cmd = new GenerateTransactionCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, $transaction, $raw);
  }

  /**
   * @param Transaction datos de las transacción que se genera.
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un TransactionResult.
   *
   * @return AluPays\Entity\TransactionResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un TransactionResult.
   */
  public function generateAndAuthorizeTransaction(Transaction $transaction, $raw = false)
  {
      $cmd = new GenerateAndAuthorizeTxCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, $transaction, $raw);
  }

  /**
   * @param ProfileAction datos del perfil a modificar.
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un MessageResult.
   *
   * @return AluPays\Entity\MessageResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un MessageResult.
   */
  public function editProfile(ProfileAction $profile_action, $raw = false)
  {
      $cmd = new EditProfileCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, $profile_action, $raw);
  }

  /**
   * @param Transaction datos de las transacción que se captura.
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un TransactionResult.
   *
   * @return AluPays\Entity\TransactionResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un TransactionResult.
   */
  public function captureTransaction(Transaction $transaction, $raw = false)
  {
      $cmd = new CaptureTransactionCmd(
            $this->http_client,
            $this->getURL()
      );

      return $cmd->execute($transaction, $raw);
  }

  /**
   * @param Transaction datos de las transacción que se reintegra.
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un TransactionResult.
   *
   * @return AluPays\Entity\TransactionResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un TransactionResult.
   */
  public function refundTransaction(Transaction $transaction, $raw = false)
  {
      $cmd = new RefundTransactionCmd(
            $this->http_client,
            $this->getURL()
      );

      return $cmd->execute($transaction, $raw);
  }

  /**
   * @param $currency. Moneda en la que se generará la transacción.
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un string.
   *
   * @return string o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un string.
   */
  public function generatePaymentToken($currency = 'USD', $raw = false)
  {
      $cmd = new GenerateTokenCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, $currency, $raw);
  }

  /**
   * @param Transaction datos de las transacción que se genera.
   * @param string $sendToEmail la dirección de email a la cual enviar el link de pago.
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un PaymentLinkResult.
   *
   * @return AluPays\Entity\PaymentLinkResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un PaymentLinkResult.
   */
  public function generatePaymentLink(Transaction $transaction, $sendToEmail, $raw = false)
  {
      $cmd = new GeneratePaymentLinkCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, [
        'transaction' => $transaction,
        'email' => $sendToEmail,
      ], $raw);
  }

  /**
   * @param string $token el token del link de pago a reenviar.
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un PaymentLinkResult.
   *
   * @return AluPays\Entity\PaymentLinkResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un PaymentLinkResult.
   */
  public function resendPaymentLink($token, $raw = false)
  {
      $cmd = new ResendPaymentLinkCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, $token, $raw);
  }

  /**
   * @param string $token el token del link de pago que se dejará de seguir.
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un MessageResult.
   *
   * @return AluPays\Entity\MessageResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un MessageResult.
   */
  public function deletePaymentLink($token, $raw = false)
  {
      $cmd = new DeletePaymentLinkCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, $token, $raw);
  }

  /**
   * @param array Las opciones para ser pasadas por get. Pueden ser:
   * - internal_transaction_id: El id interno de la transacción. El utilizado por
   *   el sistema propietario de la agencia que utiliza este SDK.
   * - transaction_id: El id de transacción generado por aluPays.
   * - booking_code: El código de reserva (interno).
   * - startdate: A partir de la fecha (en formato ISO y en zona horaria UTC).
   * - enddate: Hasta la fecha (en formato ISO y en zona horaria UTC).
   * - total_per_page: Cantidad máxima de resultados que se deben devolver (por defecto sin límite).
   * - page: Página de resultados que se desea obtener (por defecto es 1).
   * - order: Sobre qué campos ordenar, acepta los campos:
   *                - transaction_id,
   *                - date,
   *                - currency,
   *                - amount,
   *                - state
   * - dir: Solo se aplica si se define "order". Puede adquirir los valores:
   *                - asc: En este caso se ordena ascendentemente,
   *                - desc: En este caso se ordena descendentemente
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un TransactionResult.
   *
   * @return AluPays\Entity\TransactionResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un TransactionResult.
   */
  public function queryTransaction(array $options, $raw = false)
  {
      $cmd = new QueryTransactionCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, $options, $raw);
  }

  /**
   * @param array Las opciones para ser pasadas por get. Pueden ser:
   * - q: Una cadena de texto para buscar sobre el campo de email o sobre tokens identificadores de pagos.
   * - total_per_page: Cantidad máxima de resultados que se deben devolver (por defecto sin límite).
   * - page: Página de resultados que se desea obtener (por defecto es 1).
   * - order: Sobre qué campos ordenar, acepta los campos:
   *                - id,
   *                - date,
   *                - email,
   *                - state
   * - dir: Solo se aplica si se define "order". Puede adquirir los valores:
   *                - asc: En este caso se ordena ascendentemente,
   *                - desc: En este caso se ordena descendentemente
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un arreglo de PaymentLinkResult.
   *
   * @return AluPays\Entity\PaymentLinkResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un PaymentLinkResult.
   */
  public function queryPaymentLinks(array $options, $raw = false)
  {
      $cmd = new QueryPaymentLinkCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, $options, $raw);
  }

  /**
   * @param array Las opciones para ser pasadas por get. Pueden ser:
   * - request_id: El id de solicitud generado por aluPays.
   * - startdate: A partir de la fecha (en formato ISO y en zona horaria UTC).
   * - enddate: Hasta la fecha (en formato ISO y en zona horaria UTC).
   * - total_per_page: Cantidad máxima de resultados que se deben devolver (por defecto sin límite).
   * - page: Página de resultados que se desea obtener (por defecto es 1).
   * - order: Sobre qué campos ordenar, acepta los campos:
   *                - id,
   *                - date,
   *                - currency,
   *                - state
   * - dir: Solo se aplica si se define "order". Puede adquirir los valores:
   *                - asc: En este caso se ordena ascendentemente,
   *                - desc: En este caso se ordena descendentemente
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un arreglo de WithdrawalRequestResult.
   *
   * @return AluPays\Entity\WithdrawalRequestResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un WithdrawalRequestResult.
   */
  public function queryWithdrawalRequests(array $options, $raw = false)
  {
      $cmd = new QueryWithdrawalRequestCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, $options, $raw);
  }

  /**
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna una cadena de texto con una
   * url de configuración.
   *
   * @return string o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un string.
   */
  public function queryWithdrawalSettings($raw = false)
  {
      $cmd = new QueryWithdrawalSettingsCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, null, $raw);
  }

  /**
   * @param array Las opciones para ser pasadas por get. Pueden ser:
   * - mov_id: El id interno del movimiento. El utilizado por
   *   el sistema propietario de la agencia que utiliza este SDK.
   * - currency: La moneda en la cual está expresada el movimiento.
   * - startdate: A partir de la fecha (en formato ISO y en zona horaria UTC).
   * - enddate: Hasta la fecha (en formato ISO y en zona horaria UTC).
   * - total_per_page: Cantidad máxima de resultados que se deben devolver (por defecto sin límite).
   * - page: Página de resultados que se desea obtener (por defecto es 1).
   * - order: Sobre qué campos ordenar, acepta los campos:
   *                - mov_id,
   *                - date,
   *                - currency,
   *                - amount
   * - dir: Solo se aplica si se define "order". Puede adquirir los valores:
   *                - asc: En este caso se ordena ascendentemente,
   *                - desc: En este caso se ordena descendentemente
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un AccountMovResult.
   *
   * @return AluPays\Entity\AccountMovResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un AccountMovResult.
   */
  public function queryAccountMovs(array $options, $raw = false)
  {
      $cmd = new QueryAccountMovsCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, $options, $raw);
  }

  /**
   * @param array Las opciones para ser pasadas por get. Pueden ser:
   * - account_id: El id interno de la cuenta. El utilizado por
   *   el sistema propietario de la agencia que utiliza este SDK.
   * - currency: La moneda en la cual está expresada la cuenta.
   * - startdate: A partir de la fecha (en formato ISO y en zona horaria UTC).
   * - enddate: Hasta la fecha (en formato ISO y en zona horaria UTC).
   * - total_per_page: Cantidad máxima de resultados que se deben devolver (por defecto sin límite).
   * - page: Página de resultados que se desea obtener (por defecto es 1).
   * - order: Sobre qué campos ordenar, acepta los campos:
   *                - account_id,
   *                - date,
   *                - currency,
   *                - balance
   * - dir: Solo se aplica si se define "order". Puede adquirir los valores:
   *                - asc: En este caso se ordena ascendentemente,
   *                - desc: En este caso se ordena descendentemente
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un AccountMovResult.
   *
   * @return AluPays\Entity\AccountResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un AccountResult.
   */
  public function queryAccounts(array $options, $raw = false)
  {
      $cmd = new QueryAccountsCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, $options, $raw);
  }

  /**
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un ProfileResult.
   *
   * @return AluPays\Entity\ProfileResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un ProfileResult.
   */
  public function queryProfile($raw = false)
  {
      $cmd = new QueryProfileCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, [], $raw);
  }

  /**
   * @param bool por defecto es false, si es verdadero se retorna la salida
   * en formato JSON, caso contrario se retorna un StatsResult.
   *
   * @return AluPays\Entity\StatsResult o JSON dependiendo del parámetro $raw.
   * Por defecto retorna un StatsResult.
   */
  public function queryStats($raw = false)
  {
      $cmd = new QueryStatsCmd(
            $this->http_client,
            $this->getURL()
      );

      return $this->execute($cmd, [], $raw);
  }

  protected function execute(AbstractCommand $cmd, $params, $raw=false)
  {
      $result = $cmd->execute($params, $raw);
      $this->response = $cmd->getResponse();

      return $result;
  }
}
