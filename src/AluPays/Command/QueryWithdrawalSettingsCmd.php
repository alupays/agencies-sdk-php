<?php
namespace AluPays\Command;

class QueryWithdrawalSettingsCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de configuración de cobros.
   */
  const API_ENTRYPOINT = 'withdrawal/settings';

  public function execute($options, $raw = false) {
    $response = $this->http_client->get($this->base_url, self::API_ENTRYPOINT, $options);
    $ret = $response->then(function ($result) use ($raw) {
        if (!$raw) {
            $url = '';
            if (!empty($result['result']['url'])) {
              $url = $result['result']['url'];
            }
            return $url;
        } else {
            return $result['result'];
        }
    });
    $this->response = $response->getRawResponse();

    return $ret;
  }
}
