<?php

namespace AluPays\Http;

use AluPays\Authentication\APICredential;
use GuzzleHttp\Client;
use AluPays\Http\JSONResult;
use \AluPays\Core\AluPaysConstants;

/**
 * Cliente HTTP utilizado para las consultas REST de la API.
 */
class HttpClient
{
    /**
     * @var \AluPays\Authentication\APICredential credenciales de la API.
     */
    protected $api_credential = null;

    /**
     * @var \GuzzleHttp\Client Cliente HTTP.
     */
    protected $client = null;

    public function __construct(APICredential $api_credential)
    {
        $this->api_credential = $api_credential;
    }

    public function post($base_url, $entry_point, $data, $request_timeout = 60)
    {
        return new JSONResult($this->request('POST', $base_url, $entry_point, $request_timeout, [
                'body' => $data,
        ]));
    }

    public function put($base_url, $entry_point, $data, $request_timeout = 60)
    {
        return new JSONResult($this->request('PUT', $base_url, $entry_point, $request_timeout, [
                'body' => $data,
        ]));
    }

    public function delete($base_url, $entry_point, $data, $request_timeout = 60)
    {
        return new JSONResult($this->request('DELETE', $base_url, $entry_point, $request_timeout, [
                'body' => $data,
        ]));
    }

    public function get($base_url, $entry_point, $query_params, $request_timeout = 60)
    {
        return new JSONResult($this->request('GET', $base_url, $entry_point, $request_timeout, [
                'query' => $query_params,
        ]));
    }

    protected function request($method, $base_url, $entry_point, $request_timeout, array $data)
    {
        if (!array_key_exists('headers', $data) or empty($data['headers'])) {
            $data['headers'] = $this->buildRequestHeaders();
        }

        $this->setClient($base_url, $request_timeout);

        return $this->getClient()->request($method, $entry_point, $data);
    }

    public function &getClient()
    {
        return $this->client;
    }

    private function &setClient($base_url, $request_timeout, $errors = false)
    {
        $this->client = new Client([
                'http_errors' => $errors,
                'base_uri' => $base_url,
                'timeout' => $request_timeout,
        ]);

        return $this->client;
    }

    private function buildRequestHeaders()
    {
        return [
          'Authorization' => $this->api_credential->getAccessToken(),
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
		  'User-Agent' => 'aluPays SDK - '.AluPaysConstants::VERSION
        ];
    }
}
