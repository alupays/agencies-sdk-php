<?php

namespace AluPays\Entity;

use Exception;

/**
 * Acción sobre perfil de usuario.
 */
class ProfileAction
{
    /**
     * @const string Tipo "actualización de datos" para el perfil de usuario.
     */
    const T_UPDATE_DATA = 'UPDATE_DATA';
    /**
     * @const string Tipo "actualización de email" para el perfil de usuario.
     */
    const T_UPDATE_EMAIL = 'UPDATE_EMAIL';
    /**
     * @const string Tipo "actualización de contraseña" para el perfil de usuario.
     */
    const T_UPDATE_PASSWORD = 'UPDATE_PASSWORD';

    protected $type = null;
    protected $email = null;
    protected $password = null;
    protected $notification_email = null;
    protected $notify_by_email = null;
    protected $notification_sms = null;
    protected $notify_by_sms = null;

    protected $agency_fantasy_name = null;
    protected $agency_legal_name = null;
    protected $agency_address = null;
    protected $agency_logo = null;

    private function validateActionType($type)
    {
        return (in_array(strtoupper($type), [
          self::T_UPDATE_DATA,
          self::T_UPDATE_EMAIL,
          self::T_UPDATE_PASSWORD
        ]));
    }

    /**
     * @param string $type
     */
    public function __construct($type)
    {
        if (!$this->validateActionType($type)) {
            throw new Exception('[INVALID_ACTION] El tipo de la acción que se desea aplicar sobre el perfil no es válido.');
        }
        $this->type = $type;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function &setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getNotificationEmail()
    {
        return $this->notification_email;
    }

    public function &setNotificationEmail($notification_email)
    {
        $this->notification_email = $notification_email;
        return $this;
    }

    public function getNotifyByEmail($notify_by_email)
    {
        return $this->notify_by_email = $notify_by_email;
    }

    public function &setNotifyByEmail($notify_by_email)
    {
        $this->notify_by_email = $notify_by_email;
        return $this;
    }

    public function getNotificationSMS()
    {
        return $this->notification_sms;
    }

    public function &setNotificationSMS($notification_sms)
    {
        $this->notification_sms = $notification_sms;
        return $this;
    }

    public function getNotifyBySMS($notify_by_sms)
    {
        return $this->notify_by_sms = $notify_by_sms;
    }

    public function &setNotifyBySMS($notify_by_sms)
    {
        $this->notify_by_sms = $notify_by_sms;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function &setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function getAgencyFantasyName()
    {
        return $this->agency_fantasy_name;
    }

    public function &setAgencyFantasyName($agency_fantasy_name)
    {
        $this->agency_fantasy_name = $agency_fantasy_name;
        return $this;
    }

    public function getAgencyLegalName()
    {
        return $this->agency_legal_name;
    }

    public function &setAgencyLegalName($agency_legal_name)
    {
        $this->agency_legal_name = $agency_legal_name;
        return $this;
    }

    public function getAgencyAddress()
    {
        return $this->agency_address;
    }

    public function &setAgencyAddress($agency_address)
    {
        $this->agency_address = $agency_address;
        if (empty($this->agency_address)) {
          $this->agency_address = null;
        }
        return $this;
    }

    public function getAgencyLogo()
    {
        return $this->agency_logo;
    }

    public function &setAgencyLogo($agency_logo)
    {
        $this->agency_logo = $agency_logo;
        if (empty($this->agency_logo)) {
          $this->agency_logo = null;
        }
        return $this;
    }

    public function toArray()
    {
        return [
         'action' => $this->type,
         'user' => [
            'email' => $this->email,
            'password' => $this->password,
            'notification_email' => $this->notification_email,
            'notification_sms' => $this->notification_sms,
            'notify_by_email' => $this->notify_by_email,
            'notify_by_sms' => $this->notify_by_sms
          ],
          'agency' => [
            'fantasy_name' => $this->agency_fantasy_name,
            'legal_name' => $this->agency_legal_name,
            'address' => $this->agency_address,
            'logo' => $this->agency_logo
          ]
        ];
    }

    public function toJSON()
    {
        return json_encode($this->toArray());
    }
}
