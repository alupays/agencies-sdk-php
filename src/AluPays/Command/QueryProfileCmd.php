<?php
namespace AluPays\Command;

use AluPays\Entity\ProfileResult;
use AluPays\Entity\Agency;
use AluPays\Entity\User;
use AluPays\Exception\AluPaysException as Exception;

class QueryProfileCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de perfil de usuario.
   */
  const API_ENTRYPOINT = 'user/profile';

  public function execute($options, $raw = false) {
    $response = $this->http_client->get($this->base_url, self::API_ENTRYPOINT, $options);
    $retTx = $response->then(function ($results) use ($raw) {
            if (!$raw) {
                $result = new ProfileResult();
                if (!empty($results['result']['agency'])) {
                  $agency = new Agency();
                  $agency
                    ->setFantasyName($results['result']['agency']['fantasy_name'])
                    ->setLegalName($results['result']['agency']['legal_name'])
                    ->setLogo($results['result']['agency']['logo'])
                    ->setAddress($results['result']['agency']['address']);

                  $result->setAgency($agency);
                }
                if (!empty($results['result']['user'])) {
                  $user = new User();
                  $user
                    ->setUsername($results['result']['user']['username'])
                    ->setEmail($results['result']['user']['email'])
                    ->setNotificationEmail($results['result']['user']['notification_email'])
                    ->setNotifyByEmail($results['result']['user']['notify_by_email'])
                    ->setNotificationSMS($results['result']['user']['notification_sms'])
                    ->setNotifyBySMS($results['result']['user']['notify_by_sms']);

                  $result->setUser($user);
                }

                return $result;
            } else {
                return $results['result'];
            }
      });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
