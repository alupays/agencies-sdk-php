<?php
namespace AluPays\Command;

use AluPays\Entity\AccountMovResult;
use AluPays\Exception\AluPaysException as Exception;

class QueryAccountMovsCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de movimientos.
   */
  const API_ENTRYPOINT = 'account/movements';

  public function execute($options, $raw = false) {
    $response = $this->http_client->get($this->base_url, self::API_ENTRYPOINT, $options);
    $retTx = $response->then(function ($results) use ($raw) {
            if (!$raw) {
                $retTxs = [
                  'summary' => [
                    'total' => 0,
                    'page' => 1,
                    'total_per_page' => null
                  ],
                  'account_movs' => []
                ];

                foreach ($results['result']['collection'] as $result) {
                    $retTx = new AccountMovResult(
                      $result['account_mov']['id'],
                      $result['agency_account']
                    );
                    $retTx->setAmount($result['account_mov']['amount']);
                    $retTx->setCreated($result['account_mov']['created']);
                    $retTx->setDescription($result['account_mov']['description']);
                    $retTx->setLongDescription($result['account_mov']['long_description']);

                    $retTxs['account_movs'][] = $retTx;
                }

                $retTxs['summary']['total'] = $results['result']['total'];
                $retTxs['summary']['page'] = $results['result']['page'];
                $retTxs['summary']['total_per_page'] = $results['result']['total_per_page'];

                return $retTxs;
            } else {
                return $results['result'];
            }
      });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
