<?php
namespace AluPays\Command;

use AluPays\Entity\StatsResult;
use AluPays\Exception\AluPaysException as Exception;

class QueryStatsCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de estadísticas.
   */
  const API_ENTRYPOINT = 'stats';

  public function execute($options, $raw = false) {
    $response = $this->http_client->get($this->base_url, self::API_ENTRYPOINT, $options);
    $retTx = $response->then(function ($results) use ($raw) {
            if (!$raw) {
                $stats = new StatsResult();
                $stats->setListingsQuantity($results['result']['listings_quantity'])
                      ->setSalesQuantity($results['result']['sales_quantity'])
                      ->setTotal30Sales($results['result']['total_30_sales'])
                      ->setTotal30SalesByDay($results['result']['total_30_sales_by_day']);

                return $stats;
            } else {
                return $results['result'];
            }
      });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}

