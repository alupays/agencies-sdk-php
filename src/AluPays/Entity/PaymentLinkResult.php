<?php

namespace AluPays\Entity;

/**
 * Resultado de una solicitud a la API de link de pagos.
 */
class PaymentLinkResult
{
    /**
     * @var string El token del link de pago generado.
     */
    protected $token = null;

    /**
     * @var string El email al que fué enviado el link de pago generado.
     */
    protected $email = null;

    /**
     * @var boolean Flag que indica si el el link de pago fué visto por el destinatario.
     */
    protected $has_seen = false;

    /**
     * @var string La url del link de pago generado.
     */
    protected $url = null;

    protected $created = null;

    public function __construct($token, $email, $has_seen, $url)
    {
        $this->token = $token;
        $this->email = $email;
        $this->has_seen = $has_seen;
        $this->url = $url;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getHasSeen()
    {
        return $this->has_seen;
    }

    public function getURL()
    {
        return $this->url;
    }

    public function &setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }
}
