<?php
namespace AluPays\Command;

use AluPays\Entity\Transaction;
use AluPays\Entity\TransactionResult;
use AluPays\Entity\Booking;
use AluPays\Exception\AluPaysException as Exception;

class GenerateAndAuthorizeTxCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de transacciones.
   */
  const API_ENTRYPOINT = 'transaction';

  public function execute($transaction, $raw = false) {
    if (empty($transaction) && $transaction->getNonce() !== null) {
        throw new Exception('Debe suministrar una transacción y debe estar preautorizada por el operador de pago');
    }

    $transaction->setType(Transaction::TX_AUTHORIZATION);

    $response = $this->http_client->post($this->base_url, self::API_ENTRYPOINT, $transaction->toJSON());
    $retTx = $response->then(function ($result) use ($raw) {
          if (!$raw) {
              $retTx = new TransactionResult(
                $result['result']['transaction']['id'],
                $result['result']['transaction']['internal_id'],
                $result['result']['transaction']['status']
              );
              if (!empty($result['result']['payment_url'])) {
                $retTx->setPaymentUrl($result['result']['payment_url']);
              }

              return $retTx;
          } else {
              return $result['result'];
          }
    }, function ($result, $rawResponse) {
          if (!empty($result['message'])) {
            return $result['message'];
          } else {
            return $rawResponse;
          }
    });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
