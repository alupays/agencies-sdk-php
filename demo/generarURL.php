<?php
/**
 * Demo de utilización del SDK de la API de Agencias.
 */
require_once '../vendor/autoload.php';

use AluPays\Entity\Transaction;
use AluPays\Entity\Booking;
use AluPays\Entity\Listing;
use AluPays\Entity\ListingField;
use AluPays\Entity\Customer;
use AluPays\Entity\CustomerField;
use AluPays\Entity\InvoiceItem;
use AluPays\AluPaysAPIClient;

$transaction = new Transaction('apurl_'.date('c'), 'USD', 65);
$transaction->addInvoiceItem(new InvoiceItem('Adelanto extensión "Juncal & Suipacha"', 65));

$booking = new Booking('12326');
$booking->setCheckin(new DateTime('2016-06-20'));
$booking->setCheckout(new DateTime('2016-06-24'));

$customer = new Customer('jccastro44@yahoo.com.ar');
$customer->addField(new CustomerField('nombre', 'Juan Carlos Castro'));
$customer->addField(new CustomerField('pasaporte', '4969994'));
$customer->addField(new CustomerField('email', 'jccastro44@yahoo.com.ar'));

$booking->setCustomer($customer);

$listing = new Listing('442');
$listing->addField(new ListingField('Capacidad', '4', ListingField::LF_INT));
$listing->addField(new ListingField('Desde', '20/06/2016', ListingField::LF_TEXT));
$listing->addField(new ListingField('Hasta', '24/06/2016', ListingField::LF_TEXT));
$listing->addField(new ListingField('Barrio', 'Recoleta', ListingField::LF_TEXT));
$listing->addField(new ListingField('Juncal & Suipacha', 'http://www.welcome2ba.com/FOTOS/dto000442/1_buenos_aires_rentals.jpg', ListingField::LF_IMAGE));

$transaction->setBooking($booking);
$transaction->setListing($listing);

$bLive = true;

if ($bLive)
{
	$apiClient = new AluPaysAPIClient('yzSkn9o15lyzR', 'YQug6sTBtMZ8Q7mxqlihoMoQ3RUXf708199fJxJ6');
	$apiClient->setLiveMode();
}
else
{
	$apiClient = new AluPaysAPIClient('56f00a30813c6', '0cfbd4980a5621c84fa2c5e3e0009d401a373256');
	$apiClient->setSandboxMode();
}

echo " Generando URL en ".($bLive ? 'PRODUCCION':'SANDBOX')."...".PHP_EOL;
$retTx = new AluPays\Entity\TransactionResult($id, $internal_id, $status);
$retTx = $apiClient->generateTransaction($transaction);

ECHO "TRX aluPays: ".$retTx->getId().PHP_EOL;
ECHO "URL de Pago: ".$retTx->getPaymentUrl().PHP_EOL;
