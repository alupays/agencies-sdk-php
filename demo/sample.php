<?php
/**
 * Demo de utilización del SDK de la API de Agencias.
 */
require_once '../vendor/autoload.php';

use AluPays\Entity\Transaction;
use AluPays\Entity\Booking;
use AluPays\Entity\Listing;
use AluPays\Entity\ListingField;
use AluPays\Entity\Customer;
use AluPays\Entity\CustomerField;
use AluPays\Entity\InvoiceItem;
use AluPays\AluPaysAPIClient;

$codigoAgencia = 'w2ba_2';
$codigoReserva = '9999';
$anuncioID = '15784';
$transaction = new Transaction($codigoAgencia, 'USD', 360 * 1.05);
$transaction->setCallbackUrls([
  'ipn' => 'http://localhost/notifypayment',
  'success' => 'http://localhost/success',
  'fail' => 'http://localhost/fail',
]);
$transaction->addInvoiceItem(new InvoiceItem('Anticipo para "6 dias" ', 300));
$transaction->addInvoiceItem(new InvoiceItem('Gastos Administrativos', 45));
$transaction->addInvoiceItem(new InvoiceItem('Late Night (18hs)', 15));
$transaction->addInvoiceItem(new InvoiceItem('Costos de Procesamiento', 360 * .05));

$booking = new Booking('9999');
$booking->setCheckin(new DateTime('2016-09-08T08:00:00+03:00'));
$booking->setCheckout(new DateTime('2016-09-18T08:00:00+03:00'));

$customer = new Customer('2');
$customer->addField(new CustomerField('Nombre', 'Matias Perrone'));
$customer->addField(new CustomerField('Dirección', 'Av. Bernardo Ader 3864, Villa Adelina, Buenos Aires, Argentina'));

$booking->setCustomer($customer);

$listing = new Listing($anuncioID);
$listing->addField(new ListingField('l_image', 'http://static.sunnysfair.com/FOTOS/dto000436/1_rent.jpg', ListingField::LF_IMAGE));
$listing->addField(new ListingField('l_title', 'Hermosa Decoración en Recoleta', ListingField::LF_TEXT));
$listing->addField(new ListingField('Ubicación', 'Ciudad Autónoma de Buenos Aires, Buenos Aires, Argentina', ListingField::LF_TEXT));
$listing->addField(new ListingField('Tipo alojamiento', 'Casa/Depto entero', ListingField::LF_TEXT));
$listing->addField(new ListingField('Huéspedes', 2, ListingField::LF_INT));

$transaction->setBooking($booking);
$transaction->setListing($listing);

$apiClient = new AluPaysAPIClient('56f00a30813c6', '0cfbd4980a5621c84fa2c5e3e0009d401a373256');
$apiClient->setSandboxMode();

echo "==========================================================\n";
echo " GENERATE TRANSACTION \n";
echo "==========================================================\n";
echo "RESPONSE (NO RAW) :\n";
/*
    Dado un objeto AluPays\Entity\Transaction, genera una transacción y retorna
    un objeto AluPays\Entity\TransactionResult con los resultados de la transacción generada.
*/
$retTx = $apiClient->generateTransaction($transaction);
echo print_r($retTx, true);
echo "==========================================================\n";
/*
    Dado un objeto AluPays\Entity\Transaction, genera una transacción y retorna un objeto JSON
    con los resultados de la transacción generada.
*/
echo "RESPONSE (RAW): \n".print_r($apiClient->generateTransaction($transaction, true), true);
echo "\n==========================================================\n";
echo " QUERY TRANSACTION \n";
echo "\n==========================================================\n";
/*
    Emite una consulta de transacción por el id interno de una transacción (el
    utilizado en el sistema interno de cada agencia para identificar una
    transacción), y retorna un arreglo de objetos AluPays\Entity\TransactionResult
    con la información mínima sobre la transacción buscada.
*/
$results = $apiClient->queryTransaction(['internal_transaction_id' => $codigoAgencia]);
// $results = $apiClient->queryTransaction(['transaction_id' => $retTx->getId()]);
// $results = $apiClient->queryTransaction(['booking_code' => $codigoReserva]);
echo "RESPONSE (NO RAW): \n";
echo print_r($results, true);
echo "\n==========================================================\n";
/*
    Emite una consulta de transacción por el id interno de una transacción (el
    utilizado en el sistema interno de cada agencia para identificar una
    transacción), y retorna un arreglo de objetos JSON con la información mínima
    sobre la transacción buscada.
*/
echo "RESPONSE (RAW): \n".print_r($apiClient->queryTransaction(['internal_transaction_id' => $codigoAgencia], true), true);
// echo "RESPONSE (RAW): \n".print_r($apiClient->queryTransaction(['transaction_id' => $retTx->getId()], true), true);
// echo "RESPONSE (RAW): \n".print_r($apiClient->queryTransaction(['booking_code' => $codigoReserva], true), true);
echo "\n==========================================================\n";
echo $retTx->getPaymentUrl().PHP_EOL;