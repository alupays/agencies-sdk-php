<?php
namespace AluPays\Command;

use AluPays\Exception\AluPaysException as Exception;

class GenerateTokenCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de generación de token para solicitar autorización de pagos en transacciones.
   */
  const API_ENTRYPOINT = 'transaction/token';

  public function execute($params, $raw = false) {
    if (empty($params)) {
        throw new Exception('Debe suministrar una moneda.');
    }

    $response = $this->http_client->get($this->base_url, self::API_ENTRYPOINT."/$params", null);
    $retTx = $response->then(function ($result) use ($raw) {
        return (!$raw ? $result['result']['token'] : $result['result']);
    }, function ($result, $rawResponse) {
        if (!empty($result['message'])) {
          return $result['message'];
        } else {
          return $rawResponse;
        }
    });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}

