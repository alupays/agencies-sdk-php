<?php
/**
 * Demo de utilización del SDK de la API de Agencias.
 */
require_once '../vendor/autoload.php';

use AluPays\Entity\Transaction;
use AluPays\Entity\Booking;
use AluPays\Entity\Listing;
use AluPays\Entity\ListingField;
use AluPays\Entity\Customer;
use AluPays\Entity\CustomerField;
use AluPays\Entity\InvoiceItem;
use AluPays\AluPaysAPIClient;

$transaction = new Transaction('1234567890', 'USD', 70.8);
$transaction->setCallbackUrls([
  'ipn' => 'http://local.dev/notifypayment',
  'success' => 'http://local.dev/success',
  'fail' => 'http://local.dev/fail',
]);
$transaction->addInvoiceItem(new InvoiceItem('item facturado 1', 10.5));
$transaction->addInvoiceItem(new InvoiceItem('item facturado 2', 24.75));

$booking = new Booking('20478');
$booking->setCheckin(new DateTime('2016-09-08T08:00:00+03:00'));
$booking->setCheckout(new DateTime('2016-09-18T08:00:00+03:00'));

$customer = new Customer('767871');
$customer->addField(new CustomerField('nombre', 'El cliente 1'));
$customer->addField(new CustomerField('dirección', 'La dirección del cliente 1'));

$booking->setCustomer($customer);

$listing = new Listing('587647');
$listing->addField(new ListingField('tasa adicional', 5.50, ListingField::LF_FLOAT));
$listing->addField(new ListingField('tasa adicional 2', 10, ListingField::LF_FLOAT));

$transaction->setBooking($booking);
$transaction->setListing($listing);

$apiClient = new AluPaysAPIClient('56f00a30813c6', '0cfbd4980a5621c84fa2c5e3e0009d401a373256');
$apiClient->setSandboxMode();

echo "==========================================================\n";
echo " GENERATE TRANSACTION \n";
echo "==========================================================\n";
/*
    Dado un objeto AluPays\Entity\Transaction, genera una transacción y retorna
    un objeto AluPays\Entity\TransactionResult con los resultados de la transacción generada.
*/
$retTx = $apiClient->generateTransaction($transaction);

ECHO "REQUEST:\n";
echo $transaction->toJSON().PHP_EOL;

echo "RESPONSE (NO RAW) :\n";
echo print_r($retTx, true);
/*echo "==========================================================\n";
/*
    Dado un objeto AluPays\Entity\Transaction, genera una transacción y retorna un objeto JSON
    con los resultados de la transacción generada.
*/
echo "RESPONSE (RAW): \n".print_r($apiClient->generateTransaction($transaction, true), true);
