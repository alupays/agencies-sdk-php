<?php

namespace AluPays\Entity;

/**
 * Item de facturación de una transacción.
 */
class InvoiceItem
{
    /**
     * @var string Nombre del item de facturación.
     */
    protected $name = null;
    /**
     * @var double El monto del item de facturación.
     */
    protected $amount = null;

    public function __construct($name, $amount)
    {
        $this->name = $name;
        $this->amount = $amount;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function toArray()
    {
        return [
          'name' => $this->name,
          'amount' => $this->amount,
        ];
    }

    public function toJSON()
    {
        return json_encode($this->toArray());
    }
}
