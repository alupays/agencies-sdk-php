<?php

namespace AluPays\Entity;

/**
 * Campo de dato de cliente.
 */
class CustomerField
{
    /**
     * @var string El nombre del campo.
     */
    protected $name = null;
    /**
     * @var mixed El valor almacenado del campo.
     */
    protected $value = null;

    public function __construct($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function toArray()
    {
        return [
         'name' => $this->name,
         'value' => $this->value,
       ];
    }

    public function toJSON()
    {
        return json_encode($this->toArray());
    }
}
