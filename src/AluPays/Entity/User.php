<?php

namespace AluPays\Entity;

class User
{
    protected $username = null;
    protected $email = null;
    protected $notification_email = null;
    protected $notify_by_email = null;
    protected $notification_sms = null;
    protected $notify_by_sms = null;

    public function getUsername()
    {
        return $this->username;
    }

    public function &setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function &setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getNotificationEmail()
    {
        return $this->notification_email;
    }

    public function &setNotificationEmail($notification_email)
    {
        $this->notification_email = $notification_email;
        return $this;
    }

    public function getNotifyByEmail($notify_by_email)
    {
        return $this->notify_by_email = $notify_by_email;
    }

    public function &setNotifyByEmail($notify_by_email)
    {
        $this->notify_by_email = $notify_by_email;
        return $this;
    }

    public function getNotificationSMS()
    {
        return $this->notification_sms;
    }

    public function &setNotificationSMS($notification_sms)
    {
        $this->notification_sms = $notification_sms;
        return $this;
    }

    public function getNotifyBySMS($notify_by_sms)
    {
        return $this->notify_by_sms = $notify_by_sms;
    }

    public function &setNotifyBySMS($notify_by_sms)
    {
        $this->notify_by_sms = $notify_by_sms;
        return $this;
    }

    public function toArray()
    {
        $out = [
          'username' => $this->getUsername(),
          'email' => $this->getEmail(),
          'notification_email' => $this->getNotificationEmail(),
          'notify_by_email' => $this->getNotifyByEmail(),
          'notification_sms' => $this->getNotificationSMS(),
          'notify_by_sms' => $this->getNotifyBySMS()
        ];
        return $out;
    }

    public function toJSON()
    {
        return json_encode($this->toArray());
    }
}
