<?php

namespace AluPays\Entity;

/**
 * Resultado consulta API de estadísticas.
 */
class StatsResult
{
    protected $listings_quantity = null;
    protected $sales_quantity = null;
    protected $total_30_sales = null;
    protected $total_30_sales_by_day = null;

    public function getListingsQuantity()
    {
        return $this->listings_quantity;
    }

    public function &setListingsQuantity($listings_quantity)
    {
        $this->listings_quantity = $listings_quantity;
        return $this;
    }

    public function getSalesQuantity()
    {
        return $this->sales_quantity;
    }

    public function &setSalesQuantity($sales_quantity)
    {
        $this->sales_quantity = $sales_quantity;
        return $this;
    }

    public function getTotal30Sales()
    {
        return $this->total_30_sales;
    }

    public function &setTotal30Sales($total_30_sales)
    {
        $this->total_30_sales = $total_30_sales;
        return $this;
    }

    public function getTotal30SalesByDay()
    {
        return $this->total_30_sales_by_day;
    }

    public function &setTotal30SalesByDay($total_30_sales_by_day)
    {
        $this->total_30_sales_by_day = $total_30_sales_by_day;
        return $this;
    }
}
