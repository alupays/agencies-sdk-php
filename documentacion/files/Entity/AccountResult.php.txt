<?php

namespace AluPays\Entity;

/**
 * Resultado consulta API de cuentas.
 */
class AccountResult
{
    /**
     * @var mixed El id de aluPays de una cuenta.
     */
    protected $id = null;
    protected $currency = null;
    protected $balance = null;
    protected $created = null;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function &setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function &setBalance($balance)
    {
        $this->balance = $balance;
        return $this;
    }

    public function &setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }
}

