<?php

namespace AluPays\Entity;

/**
 * Resultado de una solicitud a la API de transacciones.
 */
class TransactionResult
{
    /**
     * @var mixed El id de aluPays de la transacción.
     */
    protected $id = null;
    /**
     * @var string El id interno de la transacción (generado por el sistema
     *             interno de la agencia que usa el SDK).
     */
    protected $internal_id = null;
    /**
     * @var string El estado de la transacción.
     */
    protected $status = null;
    /**
     * @var string La url de pago generada por la transacción.
     */
    protected $payment_url = null;
    /**
     * @var string El método de pago elegido.
     */
    protected $payment_method = null;
    /**
     * @var string El tipo de la transacción.
     */
    protected $type = null;
    /**
     * @var AluPays\Entity\Booking Los datos de la reserva enviados como resultado.
     */
    protected $booking = null;
    /**
     * @var AluPays\Entity\Listing Los datos del anuncio.
     */
    protected $listing = null;
    /**
     * @var array Los items de facturación asociados a la transacción.
     */
    protected $invoicing_items = [];

    protected $currency = null;
    protected $subtotal = null;
    protected $created = null;

    public function __construct($id, $internal_id, $status)
    {
        $this->id = $id;
        $this->internal_id = $internal_id;
        $this->status = $status;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getInternalId()
    {
        return $this->internal_id;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getType()
    {
        return $this->type;
    }

    public function &setType($type)
    {
        $this->type = strtoupper($type);
        return $this;
    }

    public function getBooking()
    {
        return $this->booking;
    }

    public function &setBooking($booking)
    {
        $this->booking = $booking;
        return $this;
    }

    public function getListing()
    {
        return $this->listing;
    }

    public function &setListing($listing)
    {
        $this->listing = $listing;
        return $this;
    }

    public function &setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function &setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function &setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
        return $this;
    }

    public function getSubtotal()
    {
        return $this->subtotal;
    }

    public function &setPaymentMethod($payment_method)
    {
        $this->payment_method = $payment_method;
        return $this;
    }

    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    public function &setInvoiceItems($invoicing_items)
    {
        $this->invoicing_items = $invoicing_items;
        return $this;
    }

    public function getInvoiceItems()
    {
        return $this->invoicing_items;
    }

    public function &setPaymentUrl($payment_url)
    {
        $this->payment_url = $payment_url;
        return $this;
    }

    public function getPaymentUrl()
    {
        return $this->payment_url;
    }
}
