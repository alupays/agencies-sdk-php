<?php
namespace AluPays\Command;

use AluPays\Exception\AluPaysException as Exception;

class EditProfileCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API para editar perfil.
   */
  const API_ENTRYPOINT = 'user/profile';

  public function execute($profile_action, $raw = false) {
    if (empty($profile_action)) {
        throw new Exception('Debe suministrar los datos del perfil.');
    }

    $response = $this->http_client->put($this->base_url, self::API_ENTRYPOINT, $profile_action->toJSON());
    $retTx = $response->then(function ($result) use ($raw) {
        return (!$raw ? $result['result']['message'] : $result['result']);
    }, function ($result, $rawResponse) {
        if (!empty($result['message'])) {
          return $result['message'];
        } else {
          return $rawResponse;
        }
    });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
