<?php
namespace AluPays\Command;

use AluPays\Entity\Transaction;
use AluPays\Entity\TransactionResult;
use AluPays\Entity\Booking;
use AluPays\Entity\InvoiceItem;
use AluPays\Entity\Listing;
use AluPays\Entity\ListingField;
use AluPays\Entity\Customer;
use AluPays\Entity\CustomerField;
use AluPays\Exception\AluPaysException as Exception;

class QueryTransactionCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de transacciones.
   */
  const API_ENTRYPOINT = 'transactions';

  public function execute($options, $raw = false) {
    $response = $this->http_client->get($this->base_url, self::API_ENTRYPOINT, $options);
    $retTx = $response->then(function ($results) use ($raw) {
            if (!$raw) {
                $retTxs = [
                  'summary' => [
                    'total' => 0,
                    'page' => 1,
                    'total_per_page' => null
                  ],
                  'transactions' => []
                ];

                foreach ($results['result']['collection'] as $result) {
                    $retTx = new TransactionResult(
                      $result['transaction']['id'],
                      $result['transaction']['internal_id'],
                      $result['transaction']['status']
                    );
                    $retTx->setSubtotal($result['transaction']['subtotal']);
                    $retTx->setCurrency($result['transaction']['currency']);
                    $retTx->setCreated($result['transaction']['created']);
                    $retTx->setPaymentMethod($result['transaction']['payment_method']);

                    $booking = new Booking($result['booking']['code']);
                    $customer = new Customer($result['booking']['customer']['id']);

                    if (!empty($result['booking']['customer']['fields'])) {
                      foreach ($result['booking']['customer']['fields'] as $field) {
                        $customer->addField(new CustomerField($field['name'], $field['value']));
                      }
                    }

                    $booking->setCustomer($customer);
                    $retTx->setBooking($booking);

                    if (!empty($result['listing'])) {
                      $listing = new Listing($result['listing']['id']);
                      foreach ($result['listing']['fields'] as $field) {
                        $listing->addField(new ListingField(
                            $field['name'],
                            $field['value'],
                            $field['type']
                        ));
                      }

                      $retTx->setListing($listing);
                    }

                    if (!empty($result['transaction']['invoicing_items'])) {
                      $invoice_items = [];
                      foreach ($result['transaction']['invoicing_items'] as $field) {
                        $invoice_items[] = new InvoiceItem(
                            $field['name'],
                            $field['amount']
                        );
                      }

                      $retTx->setInvoiceItems($invoice_items);
                    }

                    $retTxs['transactions'][] = $retTx;
                }

                $retTxs['summary']['total'] = $results['result']['total'];
                $retTxs['summary']['page'] = $results['result']['page'];
                $retTxs['summary']['total_per_page'] = $results['result']['total_per_page'];

                return $retTxs;
            } else {
                return $results['result'];
            }
      });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
