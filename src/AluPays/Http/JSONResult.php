<?php

namespace AluPays\Http;

use AluPays\Exception\AluPaysException as Exception;
use AluPays\Exception\NotAuthorizedException;

/**
 * Abstracción de resultados HTTP para simplificar el tratamiento de las respuestas.
 */
class JSONResult
{
    protected $response = null;

    public function __construct($response)
    {
        $this->response = $response;
    }

    public function getRawResponse()
    {
        return $this->response;
    }

    /**
     * @param callable Una función a ejecutarse en caso de que la operación se
     * realice correctamente. Puede retornar un valor. Recibe dos parámetros,
     * el resultado en forma de arreglo, y como segundo parámetros la respuesta
     * recibida sin procesar.
     * @param callable Una función a ejecutarse en caso de que la operación no se
     * realice correctamente. Puede retornar un valor. Recibe dos parámetros,
     * el resultado en forma de arreglo, y como segundo parámetros la respuesta
     * recibida sin procesar.
     *
     * @return mixed.
     *                El resultado depende de lo que se defina en la función $successFunc.
     */
    public function then(callable $successFunc, callable $failFunc = null)
    {
        $result = json_decode($this->response->getBody()->getContents(), true);

        switch ($this->response->getStatusCode()) {
           case '200':
           case '201':
              if (!empty($successFunc)) {
                  return call_user_func($successFunc, $result, $this->response);
              } else {
                  break;
              }
           case '401':
              throw new NotAuthorizedException(isset($result['message']) ? $result['message'] : null);
          default:
              if (!empty($failFunc)) {
                  call_user_func($failFunc, $result, $this->response);
              }
              if (isset($result['message'])) {
                  throw new Exception(print_r($result['message'], true));
              } else {
                  throw new Exception('Error desconocido al intentar realizar la operación: '.$this->response->getStatusCode());
              }
        }
    }
}
