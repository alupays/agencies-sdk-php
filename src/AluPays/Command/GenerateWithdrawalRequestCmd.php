<?php
namespace AluPays\Command;

use AluPays\Entity\WithdrawalRequest;
use AluPays\Entity\MessageResult;
use AluPays\Exception\AluPaysException as Exception;

class GenerateWithdrawalRequestCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de retiros.
   */
  const API_ENTRYPOINT = 'withdrawal/request';

  public function execute($wrequest, $raw = false) {
    if (empty($wrequest)) {
        throw new Exception('Debe suministrar una solicitud de retiro');
    }

    $response = $this->http_client->post(
      $this->base_url,
      self::API_ENTRYPOINT,
      $wrequest->toJSON()
    );

    $retTx = $response->then(function ($result) use ($raw) {
          if (!$raw) {
              return new MessageResult($result['result']['message']);
          } else {
              return $result['result'];
          }
    }, function ($result, $rawResponse) {
          if (!empty($result['message'])) {
            return $result['message'];
          } else {
            return $rawResponse;
          }
    });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
