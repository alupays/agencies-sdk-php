<?php

namespace AluPays\Entity;

use DateTime;

/**
 * Reserva.
 */
class Booking
{
    /**
     * @var string El código de la reserva.
     */
    protected $code = null;
    /**
     * @var DateTime La fecha y hora de checkin de la reserva.
     */
    protected $checkin = null;
    /**
     * @var DateTime La fecha y hora de checkout de la reserva.
     */
    protected $checkout = null;
    /**
     * @var AluPays\Entity\Customer Los datos del cliente asociado a la reserva.
     */
    protected $customer = null;
    /**
     * @var int La cantidad de pasajeros de la reserva.
     */
    protected $pax = null;

    public function __construct($code)
    {
        $this->code = $code;
    }

    public function &setCheckin(DateTime $checkin)
    {
        $this->checkin = $checkin;
    	return $this;
    }

    public function &setCheckout(DateTime $checkout)
    {
        $this->checkout = $checkout;
    	return $this;
    }

    public function &setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    	return $this;
    }

    public function &setPax($pax)
    {
    	$this->pax = (intval($pax) ? intval($pax) : null);
    	return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getCheckin()
    {
        return $this->checkin;
    }

    public function getCheckout()
    {
        return $this->checkout;
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function toArray()
    {
        return [
         'code' => $this->code,
         'checkin' => (!empty($this->checkin) ? $this->checkin->format(DateTime::ISO8601) : ''),
         'checkout' => (!empty($this->checkout) ? $this->checkout->format(DateTime::ISO8601) : ''),
         'customer' => (!empty($this->customer) ? $this->customer->toArray() : ''),
        ];
    }

    public function toJSON()
    {
        return json_encode($this->toArray());
    }
}
