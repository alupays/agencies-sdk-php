<?php
namespace AluPays\Command;

use AluPays\Entity\PaymentLinkResult;
use AluPays\Exception\AluPaysException as Exception;

class QueryPaymentLinkCmd extends AbstractCommand {
  /**
   * @const string Punto de entrada de API de links de pagos.
   */
  const API_ENTRYPOINT = 'payment/links';

  public function execute($options, $raw = false) {
    $response = $this->http_client->get($this->base_url, self::API_ENTRYPOINT, $options);
    $retTx = $response->then(function ($results) use ($raw) {
            if (!$raw) {
                $retTxs = [
                  'summary' => [
                    'total' => 0,
                    'page' => 1,
                    'total_per_page' => null
                  ],
                  'payment_links' => []
                ];

                foreach ($results['result']['collection'] as $result) {
                    $retTx = new PaymentLinkResult(
                      $result['token'],
                      $result['email'],
                      $result['has_seen'],
                      $result['url']
                    );

                    $retTx->setCreated($result['created']);
                    
                    $retTxs['payment_links'][] = $retTx;
                }

                $retTxs['summary']['total'] = $results['result']['total'];
                $retTxs['summary']['page'] = $results['result']['page'];
                $retTxs['summary']['total_per_page'] = $results['result']['total_per_page'];

                return $retTxs;
            } else {
                return $results['result'];
            }
      });
    $this->response = $response->getRawResponse();

    return $retTx;
  }
}
