<?php

namespace AluPays\Entity;

/**
 * Anuncio.
 */
class Listing
{
	/**
	 *
	 * @var string El id del anuncio.
	 */
	protected $id = null;
	/**
	 *
	 * @var string El título del anuncio.
	 */
	protected $title = null;
	/**
	 *
	 * @var string La url de la imagen principal del anuncio.
	 */
	protected $image = null;
	/**
	 *
	 * @var string La descripción del anuncio.
	 */
	protected $description = null;
	/**
	 *
	 * @var array de \AluPays\Entity\ListingField Un arreglo de campos extras asociados al anuncio.
	 */
	protected $fields = [];

	public function __construct($id)
	{
		$this->id = $id;
	}

	public function &addField(ListingField $field)
	{
		$this->fields[] = $field;
		return $this;
	}

	public function getId()
	{
		return $this->id;
	}

	public function &setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function &setImage($image)
	{
		$this->image = $image;
		return $this;
	}

	public function getImage()
	{
		return $this->image;
	}

	public function &setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function getFields()
	{
		return $this->fields;
	}

	public function toArray()
	{
		return [
					'id' => $this->id,
					'title' => $this->title,
					'image' => $this->image,
					'description' => $this->description,
					'fields' => array_map( function ($field)
					{
						return $field->toArray();
					}, $this->getFields() )];
	}

	public function toJSON()
	{
		return json_encode( $this->toArray() );
	}
}

