<?php

namespace AluPays\Entity;

/*
 * Campo de dato de un anuncio.
 */
class ListingField
{
    /**
     * @const string Tipo texto para el campo.
     */
    const LF_TEXT = 'TEXT';
    /**
     * @const string Tipo decimal para el campo.
     */
    const LF_FLOAT = 'FLOAT';
    /**
     * @const string Tipo entero para el campo.
     */
    const LF_INT = 'INT';
    /**
     * @const string Tipo enlace para el campo.
     */
    const LF_LINK = 'LINK';
    /**
     * @const string Tipo enlace de video para el campo.
     */
    const LF_VIDEO = 'VIDEO';
    /**
     * @const string Tipo imagen (enlace a imagen o embebido base64) para el campo.
     */
    const LF_IMAGE = 'IMAGE';

    /**
     * @var string El nombre del campo.
     */
    protected $name = null;
    /**
     * @var mixed El valor asignado al campo.
     */
    protected $value = null;
    /**
     * @var string El tipo del valor del campo.
     */
    protected $type = null;

    private function validateType($type)
    {
        return (in_array(strtoupper($type), [self::LF_TEXT, self::LF_FLOAT, self::LF_INT, self::LF_LINK, self::LF_VIDEO, self::LF_IMAGE]));
    }

    public function __construct($name, $value, $type)
    {
        $this->name = $name;
        $this->value = $value;
        if (!$this->validateType($type)) {
            throw new Exception('El tipo del campo del anuncio no es válido.');
        }
        $this->type = strtoupper($type);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getType()
    {
        return $this->type;
    }

    public function toArray()
    {
        return [
           'name' => $this->name,
           'value' => $this->value,
           'type' => $this->type,
        ];
    }

    public function toJSON()
    {
        return json_encode($this->toArray());
    }
}
